#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # new tests for collatz_read():
    def test_read_1(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_2(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 201)
        self.assertEqual(j, 210)

    def test_read_3(self):
        s = "900 1500\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 900)
        self.assertEqual(j, 1500)

    def test_read_4(self):
        s = "\n"
        with self.assertRaises(SystemExit) as e:
            collatz_read(s)

        self.assertIn(e.exception.code, (0, None))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_cache_lookup_1(self):
        v = collatz_eval(3236, 11463)
        self.assertEqual(v, 268)

    def test_eval_large_input_1(self):
        v = collatz_eval(956006, 956739)
        self.assertEqual(v, 352)

    def test_eval_large_input_2(self):
        v = collatz_eval(737959, 737960)
        self.assertEqual(v, 150)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 10000, 100000)
        self.assertEqual(w.getvalue(), "1 10000 100000\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 10000, 999999, 100000)
        self.assertEqual(w.getvalue(), "10000 999999 100000\n")
        
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 100, 200, 300)
        self.assertEqual(w.getvalue(), "100 200 300\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )
    
    def test_solve_1(self):
        r = StringIO("1018 10346\n1150 2807\n") 
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1018 10346 262\n1150 2807 209\n"
        )
    
    def test_solve_2(self):
        r = StringIO("116 5042\n1319 2186\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "116 5042 238\n1319 2186 180\n"
        )
    
    def test_solve_3(self):
        r = StringIO("1324 9022\n1348 7863\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1324 9022 262\n1348 7863 262\n"
        )


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
................
----------------------------------------------------------------------
Ran 16 tests in 0.107s

OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
....................
----------------------------------------------------------------------
Ran 20 tests in 0.377s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          40      0     14      0   100%
TestCollatz.py      88      0      0      0   100%
------------------------------------------------------------
TOTAL              128      0     14      0   100%

"""
