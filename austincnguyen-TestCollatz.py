#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, exec_alg

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # student tests 
    # testing collatz_eval
    def test_eval_5(self):
        v = collatz_eval(1001, 1500)
        self.assertEqual(v, 182)

    def test_eval_6(self):
        v = collatz_eval(1501, 2000)
        self.assertEqual(v, 180)
    
    def test_eval_7(self):
        v = collatz_eval(2001, 3000)
        self.assertEqual(v, 217)

    def test_eval_8(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    #testing exec_alg
    def test_exec_alg_9(self):
        v = exec_alg(1, 10)
        self.assertEqual(v, 7)

    def test_exec_alg_10(self):
        v = exec_alg(1, 20)
        self.assertEqual(v, 8)

    def test_exec_alg_11(self):
        v = exec_alg(1, 30)
        self.assertEqual(v, 19)

    # testing collatz_read
    def test_collatz_read_12(self):
        v = collatz_read("10 20")
        self.assertEqual(v, [10, 20])

    def test_collatz_read_13(self):
        v = collatz_read("20 30")
        self.assertEqual(v, [20, 30])
    
    def test_collatz_read_14(self):
        v = collatz_read("30 40")
        self.assertEqual(v, [30, 40])

    # testing collatz_print
    def test_collatz_print_15(self):
        w = StringIO()
        collatz_print(w, 2, 5, 8)
        self.assertEqual(w.getvalue(), "2 5 8\n")

    def test_collatz_print_16(self):
        w = StringIO()
        collatz_print(w, 3, 7, 17)
        self.assertEqual(w.getvalue(), "3 7 17\n")

    def test_collatz_print_17(self):
        w = StringIO()
        collatz_print(w, 4, 11, 20)
        self.assertEqual(w.getvalue(), "4 11 20\n")

    # testing collatz_solve
    def test_solve_18(self):
        r = StringIO("1 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 100 119\n")
        
    def test_solve_19(self):
        r = StringIO("1 100\n200 400\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 100 119\n200 400 144\n")
        
    def test_solve_20(self):
        r = StringIO("1 100\n200 400\n300 307\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 100 119\n200 400 144\n300 307 43\n")


    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
